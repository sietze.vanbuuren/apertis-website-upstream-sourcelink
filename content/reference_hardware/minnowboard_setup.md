+++
date = "2018-11-15"
weight = 100

title = "Minnowboard Setup"

aliases = [
    "/old-wiki/Reference_Hardware/Minnowboard_setup",
    "/old-wiki/Reference_Hardware/minnowboard_setup"
]
+++

![300px|thumb|Base Minnowboard setup with a Lilliput
screen](/images/hardware-intel.jpg)

{{% notice warning %}}
This device is no longer supported and tested.
{{% /notice %}}

## Minnowboard setup

You should have at least:

  - 1 Minnowboard
  - 1 Power adaptor
  - 1 USB to serial cable

You need to provide your own:

  - 1 microSD card. At least 4GB for fixedfunction image and 15GB for HMI image.

You may also have a:

  - Lilliput touchscreen
  - HDMI type A female to type D male adaptor

### Setup instructions

1. If the HSE (High Speed Expansion) is covered by a plate, you need to
   remove the plate.
2. [Download]({{< ref "download.md" >}}) the Intel 64-bit (amd64) target image
   and `.bmap` file.
3. Flash the image to a microSD card using command:

       sudo bmaptool copy path/to/image.img.gz /dev/mmcblk0

4. Put the SD card in the board.
5. [Plug in the serial cable](http://elinux.org/Minnowboard:MinnowMax#6-Wire_Serial_Console),
   (pin 1 gets the black wire on common serial cables) and connect to its
   serial console using:

       screen /dev/ttyUSB0 115200

6. Plug in the power supply and wait for it to boot
7. On the **first boot**, the EFI shell might appear. Type `bootx64` to
   get it to
   [boot into grub2](http://elinux.org/Minnowboard:MaxBoot#If_you_know_you_have_the_right_UEFI_setup_on_the_disk).
8. If the board continues to boot into the EFI shell, the
   [boot device order might be wrong](http://elinux.org/Minnowboard:MaxBios#Boot_Maintenance_Manager).
   - In the EFI shell, type `exit`
   - Select the “Boot Maintenance Manager”, then “Boot Options”, then “Change Boot Order” and move
   “EFI Misc Device” (the microSD card) to the top of the list.
   - Save and reboot.

{{% notice tip %}}
Make sure that the EFI is 64-bit. Type `exit` from the EFI shell and check
that ‘X64’ is present in the version string (something like
`MNW2MAX1.X64.0077.R01.1501291247`) at the top of the screen.
{{% /notice %}}

#### Touchscreen

<table>
<tbody>
<tr class="odd">
<td><p>If you have a Lilliput touchscreen, note that because it combines HDMI video and USB for the touchscreen into one connector, it needs to be used with the special cable provided:</p>
<ol>
<li>Connect the two-tailed HDMI connector to the Minnowboard (through the type A to type D adaptor)</li>
<li>Connect the USB lead to the Minnowboard</li>
<li>Connect the one-tailed HDMI connector to the Lilliput</li>
<li>Power up the Lilliput</li>
</ol>
<p>As in the diagram:</p>
<pre><code>
             | HD ---------------- &quot;HDMI&quot; |
MINNOWBOARD  | MI --\                     | SCREEN
             |      |                     |
             | USB -/                     |
</code></pre></td>
<td><p><img src="/images/hardware-intel-cables.jpg" alt="x300px|thumb|Detail of the cable setup"></p></td>
</tr>
</tbody>
</table>

### Firmware updates

Occasionally, updated firmware is released for the Minnowboard. This can
fix issues with the hardware, such as no HDMI output or both LEDs
on.
[Download it and apply it from the Intel website](https://firmware.intel.com/projects/minnowboard-max).

{{% notice info %}}
Apertis is currently being tested using the `MNW2MAX1.X64.0101.R01.1908091048`.
It is recommended that Minnowboards are updated to at least this version.
{{% /notice %}}

## Setting up a Minnowboard device for LAVA

### Overview

These instructions are to set up a PXE boot server with x86_64
stand-alone GRUB to boot a Minnowboard Max (E3825) or Turbot (E3826) in
LAVA. It will first boot with a kernel over TFTP and an NFS root file
system, then deploy a full OS image on an SD card and boot it with GRUB.

Note: Any EFI bootloader included in the OS image will not be used in
this scenario. A future improvement would be to make LAVA control the
EFI firmware directly rather than GRUB in order to boot from an
arbitrary EFI bootloader instead.

### PXE boot with GRUB

The first step is to set up the LAVA dispatcher to enable PXE boot with
a GRUB binary. There should already be a TFTP server installed as part
of the LAVA dispatcher set-up. The same server can be used to prodive
the PXE boot binary (GRUB).

Create a directory on the dispatcher:

    $ mkdir -p /srv/tftp/lava-grub2/x86_64-efi/

Install the GRUB binary with all the required modules built-in in this
directory:

    $ cd /srv/tftp/lava-grub2/x86_64-efi/
    $ wget https://images.collabora.co.uk/lava/boot/lava-grub2/x86_64-efi/core.efi

Note: To build GRUB (`core.efi` mentioned above) from source:

    $ git clone https://github.com/coreos/grub.git
    $ cd grub/
    $ ./autogen.sh
    $ ./configure --with-platform=efi --target=x86_64 --disable-werror
    $ make -j1
    $ make install
    $ grub-mkstandalone -O x86_64-efi -o core.efi --modules "tftp net efinet linux efifwsetup part_msdos part_gpt btrfs ext2 fat"

The supplied binary in the link above was built using git revision
93fb3dac4ae7a97c080d51d951d0e5a3109aaac7.

Install dns-masq on the dispatcher:

    $ apt install dnsmasq

Make it provide the GRUB binary for Minnowboards, in
/etc/dnsmasq.d/dhcp.conf:

```
# Minnowboard (x86_64)
dhcp-vendorclass=pxe-efi-x86_64,PXEClient:Arch:00007
dhcp-boot=tag:pxe-efi-x86_64,lava-grub2/x86_64-efi/core.efi
```

### Linux kernel requirements

In order to run Apertis tests, it's necessary to enable MMC drivers in
the Linux kernel which are by default disabled in x86_64_defconfig. A
suitable binary image is provided on the Apertis server:
[bzImage-4.18-mmc-igb](https://images.apertis.org/lava/kernel/amd64/bzImage-4.18-mmc-igb)

Note: To build this kernel image from source on x86:

    $ git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    $ cd linux
    $ git checkout v4.18
    $ make defconfig
    $ for c in \
        MMC \
        MMC_SDHCI \
        MMC_SDHCI_PCI \
        MMC_SDHCI_ACPI \
        MMC_WBSD \
        IGB; \
    do
        ./scripts/config --enable CONFIG_${c}
    done
    $ make olddefconfig 
    $ make -j3

### Device set-up

Use the standard UEFI firmware provided with the Minnowboards, with
settings reset to the factory defaults. The boot order needs to be set
to try PXE IPv4 and PXE IPv6 first, to make LAVA take the priority over
previously installed images on the device.

To change the boot order, on a serial console:

  - Turn the power on
  - Press F2 to stop the boot process and enter the firmware menu
  - Enter the Boot Maintenance Manager menu
  - Enter the Boot Options menu
  - Choose Change Boot Order
  - Move the EFI IPv4 and EFI IPv6 boot options to the top
  - Press F10 to save and Y to confirm
  - Exit the menu and restart the system

This should then automatically try PXE boot and launch GRUB.

## References

  - [elinux hardware page](http://elinux.org/Minnowboard:MinnowMax)
  - [Firmware download
    page](https://firmware.intel.com/projects/minnowboard-max)
