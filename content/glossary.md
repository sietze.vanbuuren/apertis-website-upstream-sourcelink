+++
date = "2019-11-26"
lastmod = "2021-01-18"
weight = 100

title = "Glossary"

aliases = [
    "/old-wiki/Glossary"
]
+++

{{< glossary-term "application bundle" "app bundle" "bundle" >}}
A group of functionally related components (be they services, data, or
programs), installed as a unit. This matches the sense with which "app" is
typically used on mobile platforms such as Android and iOS; for example, we
would say that an Android .apk file contains a bundle. Some systems refer to
this concept as a package, but that term is strongly associated with dpkg/apt
(.deb) packages in Debian-derived systems, so we have avoided that term.

See: [Applications]({{< ref "applications.md" >}})
{{< /glossary-term >}}

{{< glossary-term "automotive domain" "AD" "blue world">}}
A security domain (potentially a virtualised OS, or a separate OS on a
separate computer) which runs automotive processes, with direct access to
hardware such as audio output or the CAN bus; contrast with the [*infotainment
domain*]({{< ref "#infotainment-domain" >}}).

See: [Inter-Domain Communication]({{< ref "inter-domain-communication.md" >}})
{{< /glossary-term >}}

{{< glossary-term "availability" >}}
The property of being accessible and usable upon demand by an authorized entity.

See: [Security]( {{< ref "security.md" >}} )
{{< /glossary-term >}}

{{< glossary-term "built-in application bundle" >}}
An [application bundle]({{< ref"#application-bundle" >}}) providing basic
user-facing functionality, presented as a modular "app" resembling a
[store application]({{< ref "#store-application" >}}).  These are part of the
system image (`/usr/Applications`), cannot be removed, and are updated by
system updates.

See: [Applications Design]({{< ref "application-design.md" >}})
{{< /glossary-term >}}

{{< glossary-term "bundle ID" >}}
The string identifying an
[application bundle]({{< ref "#application-bundle" >}}). This should take the
form of a [reversed domain name]({{< ref "#reversed-domain-name" >}}), such as
`org.apertis.Frampton` or `uk.co.collabora.OurApp`.
{{< /glossary-term >}}

{{< glossary-term "confidentiality" >}}
The property that information is not disclosed to system entities (users,
processes, devices) unless they have been authorized to access the information.

See: [Security]( {{< ref "security.md" >}} )
{{< /glossary-term >}}

{{< glossary-term "consumer–electronics domain" "CE domain" "CD" "red world" "infotainment domain" "IVI domain">}}
A security domain (potentially a virtualised OS, or a separate OS on a separate
computer) which runs the user’s infotainment processes, including downloaded
applications and processing of untrusted content such as downloaded media;
contrast with the [automotive domain]({{< ref "#automotive-domain" >}});
Apertis is one implementation of the CE domain.

See: [Inter-Domain Communication]({{< ref "inter-domain-communication.md" >}})
{{< /glossary-term >}}

{{< glossary-term "dialogue" "dialog" >}}
A specialised form of [window]({{< ref "#window" >}}) which is modal and
typically used to prompt the user for a response to a specific question (such
as ‘do you want to save changes to this document before closing’); this is used
in the
[same sense as on desktop systems](https://en.wikipedia.org/wiki/Dialog_box).
{{< /glossary-term >}}

{{< glossary-term "essential software" >}}
The [platform]({{< ref "#platform" >}}) and
[built-in applications]({{< ref "#built-in-application" >}}).

See: [Applications Design]({{< ref "application-design.md" >}})
{{< /glossary-term >}}

{{< glossary-term "executable" >}}
The on-disk representation of a [program]({{< ref "#program">}}).
{{< /glossary-term >}}

{{< glossary-term "graphical program" >}}
A [program]({{< ref "#program" >}}) with its own UI drawing surface,    managed
by the system's window manager. This matches the sense with which "application"
is traditionally used on desktop/laptop operating systems, for instance
referring to Notepad or to Microsoft Word.
{{< /glossary-term >}}

{{< glossary-term "HMI" >}}
Human Machine Interface
{{< /glossary-term >}}

{{< glossary-term "integrity" >}}
The property that data has not been changed, destroyed, or lost in an
unauthorized or accidental manner.

See: [Security]({{<ref "security.md" >}})
{{< /glossary-term >}}

{{< glossary-term "Independent Software Vendor (ISV)" >}}
An organisation or individual who produces third-party software for Apertis, in
the form of a [store application]({{< ref "#store-application" >}}). ISVs are
identified by a [reversed domain name]({{< ref "#reversed-domain-name" >}})
such as `uk.co.collabora`.
{{< /glossary-term >}}

{{< glossary-term "notification" >}}
A transient message or alert from a [process]({{< ref "#process" >}}) to a
user, displayed for a short period of time; user interaction with the
notification can launch a [dialogue]({{< ref "#dialogue" >}}) with follow-up
options for the message; if the notification is ignored it will eventually
disappear; this is used in the same sense as on desktop systems.
![Desktop Nofifications](https://afaikblog.files.wordpress.com/2014/06/banners-dissect.png)
{{< /glossary-term >}}

{{< glossary-term "OEM" >}}
A vendor such as a vehicle manufacturer who installs an Apertis
[variant]({{< ref "#variant" >}}) on their products.
{{< /glossary-term >}}

{{< glossary-term "platform" >}}
Software that is not an
[application bundle]({{< ref "#application-bundle" >}}). This includes all the
facilities used to boot up the device and perform basic system checks and
restorations. It also includes the infrastructural services on which the
applications rely, such as the session manager, window
manager, message bus and configuration storage service, and the software
libraries shared between components.

See: [Applications]({{< ref "applications.md" >}})
{{< /glossary-term >}}

{{< glossary-term "pre-installed application bundle" >}}
A [store application]({{< ref "#store-application-bundle" >}}) which could
conceivably be removed, but is installed on the device by default (e.g. weather
might be a pre-installed application).

See: [Applications]({{< ref "applications.md" >}})
{{< /glossary-term >}}

{{< glossary-term "privilege, privilege boundary" >}}
A component that is able to access data that other components cannot is said to
be privileged. If two components have different privileges – that is, at least
one of them can do something that the other cannot – then there is said to be a
privilege boundary between them.

See: [Security]({{< ref "security.md" >}} )
{{< /glossary-term >}}

{{< glossary-term "process" >}}
A running instance of a [program](i{{< ref "#program" >}}).
{{< /glossary-term >}}

{{< glossary-term "program" >}}
A runnable piece of software, which could be either a compiled binary or a
script.
{{< /glossary-term >}}

{{< glossary-term "reversed domain name" >}}
A DNS domain name controlled by an organisation or individual, written with its
components reversed, so that the conceptually largest component is first. For
example, Collabora Ltd. controls all names within the scope of
`collabora.co.uk`, so we might use `uk.co.collabora.OurApp` as the reversed
domain name of an [application bundle]({{< ref "#application-bundle" >}}). This
style of naming is used in contexts such as D-Bus, Android and Java, as well as
in Apertis.
{{< /glossary-term >}}

{{< glossary-term "store account" >}}
An account on an "app store", analogous to Google Play accounts on Android or
Apple Store accounts on iOS, not necessarily corresponding 1:1 to a
[user]({{< ref "#user" >}}).
{{< /glossary-term >}}

{{< glossary-term "store application bundle" >}}
An [application bundle]({{< ref "#application-bundle" >}}) that is not
[built-in]({{< ref "#built-in-application-bundle" >}}): that is, either a
[pre-installed application bundle]({{< ref "#pre-installed-application-bundle" >}}),
or an ordinary application that is not preinstalled.

See: [Applications]({{< ref "applications.md" >}})
{{< /glossary-term >}}

{{< glossary-term "system extension" >}}
An [application bundle]({{< ref "#application-bundle" >}}) that is not an
[graphical program]({{< ref "#graphical-program" >}}), i.e. a user-installable
bundle of content or code (services, themes, plugins, DLC, etc.) available from
an app store.

See: [Applications]({{< ref "applications.md" >}})

{{% notice note %}}
Would it be better to define system extensions in terms
of putting files in /var/lib/apertis_extensions, and say that each app bundle
may contain an agent, a graphical program, a system extension and/or future
forms of content?
{{% /notice %}}
{{< /glossary-term >}}

{{< glossary-term "system service" >}}
A background program that is run on behalf of the system as a whole, not a
specific user; normally part of the [platform]({{< ref "#platform" >}}), but
potentially part of an [application bundle]({{< ref "#application-bundle" >}}).
{{< /glossary-term >}}

{{< glossary-term "trust" "trusted computing base" "TCB" >}}
A trusted component is a component that is technically able to violate the
security model (i.e. it is relied on to enforce a privilege boundary), such
that errors or malicious actions in that component could undermine the security
model. The TCB is the set of trusted components for a particular privilege
boundary. Not automatically the same thing as being trustworthy\!

See: [Security]({{< ref "security.md" >}})
{{< /glossary-term >}}

{{< glossary-term "user" >}}
A person who uses the system.
{{< /glossary-term >}}

{{< glossary-term "user account" >}}
The software representation of a [user]({{< ref "#user" >}}).
{{< /glossary-term >}}

{{< glossary-term "uid" >}}
The numeric Unix identifier that is a property of each process, as returned by
e.g. `getuid()`, potentially representing a [user]({{< ref "#user" >}}), multiple
users, a system component and/or a subset of a user's processes.
{{< /glossary-term >}}

{{< glossary-term "user service" >}}
A background program that is run on behalf of a specific
[user]({{<ref "#user" >}}), regardless of whether it is part of the
[platform]({{< ref "#platform" >}}) like `systemd --user`, or part of an
[application bundle]({{< ref "#application-bundle" >}}).
{{< /glossary-term >}}

{{< glossary-term "variant" >}}
An [OEM]({{< ref "#oem" >}})-specific version of Apertis, with their
customisations and default applications; the UI and main interface (application
launcher, status bar, etc.) may be customised
{{< /glossary-term >}}

{{< glossary-term "window" >}}
The main user interface container for a graphical program, used in the same
sense as in
[traditional desktop UIs](https://en.wikipedia.org/wiki/Window_%28computing%29),
though perhaps rendered with different window decoration and with the system
restricted to only rendering the main window from one focused program at once.
{{< /glossary-term >}}
