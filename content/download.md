+++
date = "2020-09-02"
lastmod = "2022-04-05"
weight = 100

title = "Image Downloads"
+++

The Apertis project provides a number of images, designed for evaluation and
development purposes. Presented below are the downloads for the project's latest
stable release
({{< release-info Stable version >}}.{{< release-info Stable revision >}}),
the recommended version for evaluation purposes.

- **SDK**: The SDK is a development environment primarily designed to be run in
  [VirtualBox]({{< ref "virtualbox.md" >}}).

  | Image |
  | ----- |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 sdk >}}) |

- **FixedFunction**: A compact image, utilizing [OSTree]({{< ref
  "guides/ostree.md" >}}) for updates, designed to be run as a headless system
  on [target hardware]({{< ref "reference_hardware" >}}).

  | Image | Optional Downloads |
  | ----- | ------------------ |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 fixedfunction ostree >}}) | [bmap file]({{< image-url Stable amd64 fixedfunction ostree bmap >}}) |
  | [ARM 32-bit (armhf)]({{< image-url Stable armhf fixedfunction ostree >}}) | [bmap file]({{< image-url Stable armhf fixedfunction ostree bmap >}}) |
  | [ARM 64-bit (arm64)]({{< image-url Stable arm64 fixedfunction ostree >}}) | [bmap file]({{< image-url Stable arm64 fixedfunction ostree bmap >}}) |

  {{% notice note %}}
  This image variant was introduced in v2022dev3 as a successor to the previous
  *minimal* images.
  {{% /notice %}}

- **HMI**: A more featureful image, utilizing [OSTree]({{< ref
  "guides/ostree.md" >}}) for updates, providing an example graphical
  environment and designed to be run on [target hardware]({{< ref
  "reference_hardware" >}}).

  | Image | Optional Downloads |
  | ----- | ------------------ |
  | [Intel 64-bit (amd64)]({{< image-url Stable amd64 hmi ostree >}}) | [bmap file]({{< image-url Stable amd64 hmi ostree bmap >}}) |
  | [ARM 32-bit (armhf)]({{< image-url Stable armhf hmi ostree >}}) | [bmap file]({{< image-url Stable armhf hmi ostree bmap >}}) |
  | [ARM 64-bit (arm64)]({{< image-url Stable arm64 hmi ostree >}}) | [bmap file]({{< image-url Stable arm64 hmi ostree bmap >}}) |

  {{% notice note %}}
  This image variant was introduced in v2022dev3 as a successor to the previous
  *target* images.
  {{% /notice %}}

{{% notice info %}}
Apertis provides a wider selection of [image types]({{< ref "images.md" >}})
designed for other specific purposes and [versions]({{< ref "releases.md" >}})
at differing points in the
[release flow]({{< ref "release-flow.md#apertis-release-flow" >}}). These can
be found on the [Apertis image site](https://images.apertis.org).
{{% /notice %}}
