+++
date = "2015-10-15"
weight = 100
toc = true

title = "How to Customize GTK Widgets Used by WebKit2GTK+"

aliases = [
    "/old-wiki/WebKit2_CustomizableWidget"
]
+++

Recent gtk+ provides an useful way to style gtk widget without re-compiling.
You can cusmotize your widgets by adding css like rules to
`./Source/WebCore/css/gtk.css`. See
[GtkCssProvider](https://developer.gnome.org/gtk3/stable/GtkCssProvider.html)
for more details.

# Widgets and tips

Here are some information and tips which help you write rules in gtk.css. When
defining style rules, you can use a class name for some widgets as its
selector. For instance, it is possible to use '.button' instead of 'GtkButton'.
Please refer to
[gtk-contained.css](https://gitlab.gnome.org/GNOME/gtk/-/blob/gtk-3-24/gtk/theme/Adwaita/gtk-contained.css).
You can learn how Adwaita theme is defined and which class name is available.

## Button

An example showing how to style `GtkButton`. The default button theme of
Adwaita fill the background of the button with an image. So you need
`background-image: none;` as well where you change the background-color of the
button.

    .button {
        color: #0000ff;
        border-image: none;
        border-color: red;
        border-width: 3px;
        border-style: solid;
        background-image: none;
        background-color: yellow;
    }

    .button:hover {
        color: red;
        background-color: #00ffff;
    }

It is ok to use `GtkButton` instead.

    GtkButton {
        color: #0000ff;
        border-image: none;
        border-color: red;
        border-width: 3px;
        border-style: solid;
        background-image: none;
        background-color: yellow;
    }

## Check button & Radio button

An example showing how to style `GtkCheckButton` and `GtkRadioButton`.

    GtkCheckButton {
        color: #0000ff;
        border-image: none;
        border-color: red;
        border-width: 3px;
        border-style: solid;
        background-image: none;
        background-color: #ffff00;
        -GtkCheckButton-indicator-size: 16;
    }

You can use image formats like png to replace the default check button and
radio button.

    GtkCheckButton {
    -gtk-icon-source: -gtk-scaled(url("checkbox-unchecked.png"), url("checkbox-unchecked@2.png"));
    }

You need to remember to provide a full set of images for all conditions of the
buttons. Also remember that you define `-gtk-icon-source` once, other style
rules will be ignored.

## Scrollbar

An example showing how to style scrollbar.

    .scrollbar {
        -GtkRange-slider-width: 13;
        -GtkRange-stepper-spacing: 0;
        -GtkRange-trough-border: 1;
        -GtkRange-trough-under-steppers: 1;
        -GtkScrollbar-has-backward-stepper: true;
        -GtkScrollbar-has-forward-stepper: true;
        -GtkScrollbar-min-slider-length: 42;

        background-image: none;
        background-color: #ffff00;
    }

    .scrollbar.vertical {
        background-image: none;
        background-color: #ff00ff;
    }

    .slider {
        margin: 0;
        background-color: #0000ff;
        border: 1px solid white;
        background-clip: padding-box;
    }

## Scrolled window

I am not sure why we need this. Maybe it aims to change the scrollbar theme
inside? If that is the case, it would be better to add rules to `.scrollbar`
like [Scrollbar](#scrollbar). Of course you can add them to `GtkScrolledWindow`
for itself.

    GtkScrolledWindow {
        border-style: none;
        -GtkScrolledWindow-scrollbar-spacing: 0;
        -GtkScrolledWindow-scrollbars-within-bevel: 1;
    }

## Spin button

    GtkSpinButton {
        background-color: orange;
    }

or

    .spinbutton .button {
        background-color: orange;
    }

## Text field

Text field is `GtkEntry` in gtk term. This includes a text box, search box.

    .entry {
        color: blue;
        border-image: none;
        border-color: red;
        border-width: 3px;
        border-style: dotted;
        background-image: none;
        background-color: yellow;
    }

If you want to set a specific color to background, set `background-image: none`
as well as `GtkButton`.

## Search field results and cancel button

This is a little complex. These two buttons are not independent widgets.
Instead WebKit2GTK+ paints them inside a `GtkEntry` by using `GdkPixbuf` after
loading gtk icons.(`GTK_STOCK_FIND` and `GTK_STOCK_CLEAR`) So it seems not
possible to replace the images with others through `GtkCssProvider`. The easist
way to do that is just overwriting the default images with customized images.

## Combo box (Dropdown menu)

WebKit2GTK+ paints a combo box with combinations fo a `GtkButton`, an
`GtkArrow` and an `GtkComboBox` instead of using a stand alone `GtkComboBox`.
So by styling these three elements you can change a look of a combo box.
Possible properties are like below.

    .button {
        color: #0000ff;
        border-image: none;
        border-color: red;
        border-width: 10px;
        border-style: solid;
        background-image: none;
        background-color: yellow;
    }

    .arrow {
        color: #ff00ff;
        -GtkArrow-arrow-scaling: 1.0;
    }

    GtkComboBox {
        -GtkWidget-wide-separators: true;
        -GtkWidget-separator-width: 1px;
    }

## Tooltip

Currently there is no way to change the default look of a tooltip in
application level. Because gtk+ 3 does not allow users to access `GtkWindow` of
the tooltip so that we can not add `GtkCssProvider` to its `GtkStyleContext`.
Instead, you can do it by putting styles into a system wide `gtk.css`. In my
case using Ubuntu GNOME 15.04, it is `\~/.config/gtk-3.0/gtk.css`. See
[GtkCssProvider](https://developer.gnome.org/gtk3/stable/GtkCssProvider.html)
for more information.

    .tooltip {
        color: blue;
        background-color: yellow;
    }

## Progress bar

If you want to change the progressbar color, then set `background-image: none`
as well.

    GtkProgressBar {
        border-color: red;
        border-width: 3px;
        border-style: dotted;
        box-shadow: inset 1px 1px rgba(0, 0, 0, 0.1), 0 1px white;
        color: rgba(46, 52, 54, 0.4);
        font-size: smaller;
        padding: 0;
        -GtkProgressBar-xspacing: 0;
        -GtkProgressBar-yspacing: 0;
        -GtkProgressBar-min-horizontal-bar-height: 3;
    }

    .progressbar {
        background-color: yellow;
        background-image: none;
        border: 2px dotted #184472;
        border-radius: 1.5px;
        box-shadow: none;
    }

## Media buttons

Media buttons are controls used by video and audio player in WebKit2GTK+. They
are not native GTK+ widgets nor pure GTK+ icons unlike other controls, so you
need a little different approach to style them.  These buttons are internally
implemented with html, javascript, css and gtk symbloic icons. In other words,
WebKit2GTK builds the media controls by using web technology and just paints
gtk icons on each html region.  For the reason, you need to modify two parts to
customize the media buttons.

### How to change (foreground) color, background color

You need to change or add css style rules in
`./Source/WebCore/css/mediaControlsGtk.css` and then rebuild WebKit2GTK+. For
instance, you can change background color of the media panel to red by
manipulating a following selector in
`./Source/WebCore/css/mediaControlsGtk.css`.

    audio::-webkit-media-controls-panel, video::-webkit-media-controls-panel {
        ... (existing rules) ...
        background-color: rgba(255, 0, 0, 1);
    }

### How to change icons for each button.

The used icons for media controls are those of the default theme on your
system. If you change nothing on the theme then you might be using "Adwaita"
theme provided by `gnome-icon-theme-symbolic` package. The Adwaita is the
default theme for GTK+. So it is not recommend to change or modify the system
icons included for the theme. Instead, we provide you a useful paths to
override the icons.

    $INSTALL_PREFIX/share/webkitgtk-4.0/resources/themes/icons
    ./WebKitBuild/Release/icons (for a development purpose)

Let's suppose that you want to replace the media play button icon. Then you
should place your icon in one of following paths.

    $INSTALL_PREFIX/share/webkitgtk-4.0/resources/themes/icons/Adwaita/scalable/actions/media-playback-start-symbolic.svg
    ./WebKitBuild/Release/icons/Adwaita/scalable/actions/media-playback-start-symbolic.svg (for a development purpose.)

Be careful of using svg images and keep the path same with that of the
repalcing theme.

Full list of used icons and media button names is like following.

| Symbolic icon name            | Fallback icon name | Default icon path                                                           |
| ----------------------------- | ------------------ | --------------------------------------------------------------------------- |
| media-playback-start-symbolic | gtk-media-play     | /usr/share/icons/Adwaita/scalable/actions/media-playback-start-symbolic.svg |
| media-playback-pause-symbolic | gtk-media-pause    | /usr/share/icons/Adwaita/scalable/actions/media-playback-pause-symbolic.svg |
| audio-volume-high-symbolic    | audio-volume-high  | /usr/share/icons/Adwaita/scalable/status/audio-volume-high-symbolic.svg     |
| view-fullscreen-symbolic      | gtk-fullscreen     | /usr/share/icons/Adwaita/scalable/actions/view-fullscreen-symbolic.svg      |

### How to create svg icons for GTK+

I'm not an expert for desgin though, here I leave some information to learn how
to create svg icons for GTK+. In short, [InkScape](https://inkscape.org), a
vector drawing tool is for you.

# Further reading

- https://jonbrettdev.wordpress.com/2013/08/24/gtk-symbolic-icon-woes
- http://askubuntu.com/questions/148955/why-do-the-gnome-symbolic-icons-appear-darker-in-a-running-application/154846#154846
- https://gitlab.gnome.org/Archive/gnome-icon-theme-symbolic
