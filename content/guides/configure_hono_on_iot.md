+++
date = "2022-07-20"
weight = 100

title = "Configure IoT images to connect to Hono"
+++

The Eclipse project provides a publicly available Hono sandbox that can be used to test IoT devices that will use Kanto.
To connect to the Eclipse Hono sandbox, the device and user account must be registered.

Then, the IoT image's suite-connector configuration must be adapted to use the correct parameters.

The following sections describe how to set it all up.

# Hono Registration

Registration is done from a desktop, not from the IoT device.

Eclipse provides [a guide](https://www.eclipse.org/kanto/docs/getting-started/hono/) on how to use the Hono sandbox.
It can be followed up to the [Provision the Eclipse Hono tenant and device](https://www.eclipse.org/kanto/docs/getting-started/hono/#provision-the-eclipse-hono-tenant-and-device) section.

In order to minimize the risk of collisions of device identities and credentials and to reduce the risk of others guessing your identifiers, you are advised to use non-trivial, hard-to-guess tenant and device identifiers (e.g. a UUID).
Eclipse lists [other recommendations on using the Hono sandbox](https://www.eclipse.org/hono/sandbox/).

Once your device is registered, you are ready to configure the IoT image on the device.

# Configure the IoT Image

The configuration of the provisioning needs to be written in the `/etc/suite-connector/provisioning.json` file.

Here is an example for the provisioning configuration to connect to the Hono sandbox:

```
{
    "id": "hono-sandbox",
    "hub": {
        "credentials": {
            "tenantId": "[TENANT]",
            "type": "hashed-password",
            "enabled": true,
            "secrets": [
                {
                    "passwordBase64": "[BASE64_PASSWORD]"
                }
            ],
            "deviceId": "[DEVICE_ID]",
            "authId": "[AUTH_ID]",
            "adapters": [
                {
                    "type": "mqtt",
                    "uri": "tcp://hono.eclipseprojects.io",
                    "host": "hono.eclipseprojects.io",
                    "port": 1883
                }
            ]
        },
        "device": {
            "enabled": true,
            "deviceId": "[DEVICE_ID]"
        }
    },
    "things": {
        "thing": {
            "attributes": {
                "Info": {
                    "displayName": "cli",
                    "gateway": false
                }
            },
            "thingId": "[DEVICE_ID]",
            "policyId": "policy"
        }
    }
}
```

Once the file is created, change the following values:
 - `[TENANT]`: The Hono tenant that has been created in the previous section.
 - `[BASE64_PASSWORD]`: The Hono password that has been used in the previous section. The password must be encoded in base64. It can be obtain by running `echo -n [PASSWORD] | base64`. \*
 - `[DEVICE_ID]`: The identifier of the device on the tenant created in the previous section (Note that is has to be set at 3 places).
 - `[AUTH_ID]`: The authentication identifier of the device created in the previous section.

The file can now be saved.
Before starting the `suite-connector` service, check the following section about limitations.

\*: Note that a base64 encoding is not a hash (`base64 -d` will decode the encoded value into the password) and is mainly used here to prevent human eyes to get the password with a simple glimpse at the encoded value.

## Limitations

### No SSL

For this `demo`, no SSL is used. Beware that your Hono password might be sent over the internet without encryption.

### CA Certificate

The current version of the suite-connector program [requires a CA Certificate to be present and valid](https://github.com/eclipse-kanto/suite-connector/issues/34).
A basic certificate is provided in `/etc/suite-connector/iothub.crt` but does not do anything for now.

If you want to, you can generate a dummy `iothub.crt` file in `/etc/suite-connector/`.
To generate it, you can run:

    openssl genrsa -out ca.key 2048
    sudo openssl req -new -x509 -days 1826 -key ca.key -out /etc/suite-connector/iothub.crt

## Starting the Service

The `suite-connector` service can now be started with:

    systemctl start suite-connector.service

