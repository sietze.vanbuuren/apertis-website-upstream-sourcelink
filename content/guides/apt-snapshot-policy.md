+++
title = "APT snapshot policy"
toc = true
date = "2022-06-22"
+++

This document outlines the APT snapshot retention policy that has been defined
and implemented along with the OBS aptly publisher integration.

# Background

APT snapshot functionality is supported in Apertis using the aptly publisher
backend integrated to OBS. Snapshots are periodically created from the published
repositories increasing the storage consumption over time, see
[Repository Storage Usage](https://infrastructure.pages.apertis.org/dashboard/storage.html).

To overcome this potential storage issue, a snapshot retention mechanism is
provided to drop/retain snapshots.

# Snapshot retention policy overview

On Apertis we have multiple Stable and Development releases, where snapshots are
provided for the entire repositories. Using the default/provided policies,
Stable release snapshots are kept indefinitely, while Development release
snapshots are dropped after a certain period of time, or kept on a
daily/weekly/monthly basis.
Moreover, in certain cases a snapshot may be marked individually as an exception
to be retained indefinitely.

The new retention policy mechanism should also be flexible enough to allow each
product team to define its own set of retention policies.

## Implementation

On the OBS backend, the following script is used to run the snapshot retention
policies.

```
obsrun@backend:/srv/obs/aptly$ /usr/lib/obs/server/bs_aptly_snapshot_policy -h
reading /etc/obs/BSConfig.local.pm...
Usage: /usr/lib/obs/server/bs_aptly_snapshot_policy [OPTIONS]
  OPTIONS:
    -c <PATH>:		Configuration files path (default: `/srv/obs/aptly/snapshot.policy.d`)
    -h:			Print this help and exit
    -i <INPUT>:		Input file with published snapshots list (e.g. `aptly publish list -raw` output)
    -r:			Dry run. Print the results without actually dropping snapshots
    -t <TIMESTAMP>:	Run policy using TIMESTAMP as the current date timestamp (e.g. `20220530T135832Z`)
    -v:			Verbose debug output
```

This script should be executed periodically as a cron job or some similar
mechanism. On each run, the script goes through all the defined policy files at
`/srv/obs/aptly/snapshot.policy.d/`, applying them to the target snapshots.

### Policy configuration files

The snapshot policy configuration files are stored by default at
`/srv/obs/aptly/snapshot.policy.d/`. This path can be overwriten using the
script `-c <PATH>` option.

A snapshot retention policy must contain the target prefix and distribution to
be applied. It will retain snapshots on a daily/weekly/monthly basis, using the
specified retention time ranges. Also, individual snapshots can be marked in the
keep list to be retained indefinitely. E.g.:

#### Mandatory fields

These fields must be set in every policy.

* `prefix` (string): path where the target distribution is published.
* `distribution` (string): regex matching the target distribution. Any valid
  regular expression can be used.

#### Optional fields

The following optional fields can be set in the policy to define its behaviour.
Note that if none of the following fields are set, as there's no retention
policy defined for the matching prefix/distribution, all the related snapshots
will be dropped on each run.

* `retention::days_range` (integer): sets the policy to keep all the snapshots
  in the past `<days_range>` days.
* `retention::weeklies_range` (integer): sets the policy to keep weekly
  snapshots (first snapshot of the week) for the past `<weeklies_range>` weeks.
* `retention::monthlies_range` (integer): sets the policy to keep monthly
  snapshots (first snapshot of the month) for the past `<monthlies_range>`
  months.
* `retention::keep` (list of strings): each entry is a regex of form
  `distribution/timestamp` setting the policy to keep all the matching
  snapshots.

#### Example policy configuration

```
$ cat /srv/obs/aptly/snapshot.policy.d/apertis.v202x.yaml
prefix: shared/apertis/public/apertis
distribution: v202.*
retention:
  days_range: 7
  weeklies_range: 4
  monthlies_range: 6
  keep:
    - .*/202104.*
    - v2022/20220613T012229Z
    - v2023/20230614T142755Z
```

* Apply this policy to all snapshots from prefix `shared/apertis/public/apertis`
  and distribution matching regex `v202.*`.
* Keep all the snapshots in the past 7 days.
* Keep weekly snapshots (first snapshot of the week) for the past 4 weeks.
* Keep monthly snapshots (first snapshot of the month) for the past 6 months.
* Keep snapshots with timestamp `202104.*` for all matching distributions.
* Keep snapshots with timestamp `20220613T012229Z` for distribution `v2022`.
* Keep snapshots with timestamp `20230614T142755Z` for distribution `v2023`.
* Delete everything else.
