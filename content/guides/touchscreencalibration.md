+++
date = "2014-12-17"
weight = 100

title = "Touchscreen Calibration"
+++

By default, X will scale the touchscreen's input to the display, this typically
results in off screen areas will be included, resulting in touch sensing to be

To remove off screen areas, evdev calibration must be used. When calibration is
used, touchscreen co-ordinates outside of the calibrated area will act as if
they're off screen. moving a finger from inside the area to outside the area
will result in a release event at the edge of the screen. Moving from outside
to inside will result in a press. The intent is that the screen behave as if
the calibrated area was its physical boundary.

Setting this up for the eGalax touch screen on SABRE Lite can be done by
creating a file in `/usr/share/X11/xorg.conf.d` with the contents:

    Section "InputClass"
            Identifier "Calibrated Touch Screen"
            MatchProduct "eGalax Touch Screen"
            Driver "evdev"
            Option "Calibration" "0 16384 0 32768"
    EndSection

or, for the Iiyama touch screen on the intel reference platform:

    Section "InputClass"
            Identifier "Calibrated Touch Screen"
            MatchProduct "QUANTA OpticalTouchScreen"
            Driver "evdev"
            Option "Calibration" "0 1000 0 1080"
    EndSection

The calibration numbers are low X, high X, low Y, high Y in unscaled touch
sensor co-ordinates. X will scale touch sensor motion inside this range to fill
the display.

This can also be done at run-time without changing configuration files by doing
the following (xinput may need to be installed with `apt-get install xinput`):

    xinput "eGalax Touch Screen" "Evdev Axis Calibration" 0 16384 0 32768

If using different devices, use

    xinput list

to find the name.
