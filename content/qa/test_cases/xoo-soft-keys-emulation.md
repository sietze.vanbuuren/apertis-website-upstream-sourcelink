+++
date = "2017-10-25"
weight = 100

title = "xoo-soft-keys-emulation"

aliases = [
    "/old-wiki/QA/Test_Cases/xoo-soft-keys-emulation"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
