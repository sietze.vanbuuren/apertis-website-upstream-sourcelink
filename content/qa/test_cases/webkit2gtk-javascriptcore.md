+++
date = "2017-05-22"
weight = 100

title = "webkit2gtk-javascriptcore"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2gtk-javascriptcore"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
