+++
date = "2018-06-25"
weight = 100

title = "check-dbus-services"

aliases = [
    "/old-wiki/QA/Test_Cases/check-dbus-services"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
