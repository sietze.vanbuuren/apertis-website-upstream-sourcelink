+++
date = "2017-11-30"
weight = 100

title = "apparmor-libreoffice"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-libreoffice"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
