+++
date = "2015-09-09"
weight = 100

title = "tracker-configuration"

aliases = [
    "/old-wiki/QA/Test_Cases/tracker-configuration"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
