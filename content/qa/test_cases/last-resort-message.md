+++
date = "2018-05-14"
weight = 100

title = "last-resort-message"

aliases = [
    "/old-wiki/QA/Test_Cases/last-resort-message"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
