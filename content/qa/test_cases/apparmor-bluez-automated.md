+++
date = "2018-09-13"
weight = 100

title = "apparmor-bluez-automated"

aliases = [
    "/old-wiki/QA/Test_Cases/apparmor-bluez-automated"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
