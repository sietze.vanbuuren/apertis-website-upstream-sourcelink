+++
date = "2022-06-3"
weight = 100

title = "v2023dev2 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2023dev2** is the third **development** release of the Apertis
v2023 stable release flow that will lead to the LTS **Apertis v2023.0**
release in March 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations. It currently
ships with the Linux kernel 5.15.x LTS series but later releases in the
v2023 channel will track newer kernel versions up to the next LTS.

Test results for the v2023dev2 release are available in the following
test reports:

  - [APT images](https://qa.apertis.org/report/v2023dev2/20220608.0020/apt)
  - [OSTree images](https://qa.apertis.org/report/v2023dev2/20220608.0020/ostree)
  - [NFS artifacts](https://qa.apertis.org/report/v2023dev2/20220608.0020/nfs)
  - [LXC containers](https://qa.apertis.org/report/v2023dev2/20220608.0020/lxc)

## Release flow

  - 2021 Q4: v2023dev0
  - 2022 Q1: v2023dev1
  - **2022 Q2: v2023dev2**
  - 2022 Q3: v2023dev3
  - 2022 Q4: v2023pre
  - 2023 Q1: v2023.0
  - 2023 Q2: v2023.1
  - 2023 Q3: v2023.2
  - 2023 Q4: v2023.3
  - 2024 Q1: v2023.4
  - 2024 Q2: v2023.5
  - 2024 Q3: v2023.6
  - 2024 Q4: v2023.7

### Release downloads

| [Apertis v2023dev2.0 images](https://images.apertis.org/release/v2023dev2/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2023dev2/v2023dev2.0/amd64/fixedfunction/apertis_ostree_v2023dev2-fixedfunction-amd64-uefi_v2023dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev2/v2023dev2.0/amd64/hmi/apertis_ostree_v2023dev2-hmi-amd64-uefi_v2023dev2.0.img.gz) | [base SDK](https://images.apertis.org/release/v2023dev2/v2023dev2.0/amd64/basesdk/apertis_v2023dev2-basesdk-amd64-sdk_v2023dev2.0.ova) | [SDK](https://images.apertis.org/release/v2023dev2/v2023dev2.0/amd64/sdk/apertis_v2023dev2-sdk-amd64-sdk_v2023dev2.0.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev2/v2023dev2.0/armhf/fixedfunction/apertis_ostree_v2023dev2-fixedfunction-armhf-uboot_v2023dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev2/v2023dev2.0/armhf/hmi/apertis_ostree_v2023dev2-hmi-armhf-uboot_v2023dev2.0.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2023dev2/v2023dev2.0/arm64/fixedfunction/apertis_ostree_v2023dev2-fixedfunction-arm64-uboot_v2023dev2.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2023dev2/v2023dev2.0/arm64/fixedfunction/apertis_ostree_v2023dev2-fixedfunction-arm64-rpi64_v2023dev2.0.img.gz) | [hmi](https://images.apertis.org/release/v2023dev2/v2023dev2.0/arm64/hmi/apertis_ostree_v2023dev2-hmi-arm64-rpi64_v2023dev2.0.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2023dev2 repositories

    deb https://repositories.apertis.org/apertis/ v2023dev2 target development sdk


## New features

### Support automated license compliance for Rust packages

The process described in [Automated License Compliance]({{< ref automated-license-compliance.md >}}) has been improved
to support Rust packages to provide a more reliable license report. Since Rust packages are statically linked, the
process now captures the license of all the build dependencies to generate the package metadata.

### New reference AMD 64 hardware selected

As described in [AMD 64 reference boards]({{< ref amd64.md >}}) Apertis has chosen **UP Squared 6000** as the
new reference board to build products. This decision was done after a long research to compare different alternatives since
**MinnowBoard** boards are no longer available. Work to fully support the new board in Apertis is currently in progress.

## Build and integration

### Aptly publisher support

Based on the concept document [Cloud-friendly APT repository publishing]({{< ref "apt-publisher.md" >}})
the Apertis project now supports **aptly** as publisher for its repositories which unlocks key features such as:
- Improved publishing workflow, specially during release periods
- Support for snapshots natively
- Support for cloud storage is also in progress and should be made available soon

Newer Apertis development repositories will be published using **aptly**.

### Direct cloud storage support for aptly

As an effort to improve the features in the **aptly** publisher, changes to support direct cloud storage were proposed upstream.
This feature removes the need of a local internal working pool, reducing the local storage requirement,
avoiding duplication and reducing maintenance costs.
Although not integrated into Apertis yet, the plan is to enable the feature in a future release.

### Dashboard improvements

[Dashboard](https://infrastructure.pages.apertis.org/dashboard/)
is the tool Apertis uses to have a single view of packages' health for the different supported releases.
Newer features has been added to make it more developer friendly such as:
- Support for different domains of issues
- Support for different priorities of issues
- Support for licensing issues

### QA Report App with Gitlab issues support

As planned in [Moving to Gitlab issues]({{< ref "gitlab-issues.md" >}}) QA Report App has been improved to add
support for different bug tracking systems, including Phabricator and Gitlab issues. Thanks to this change
Apertis will soon move to Gitlab in order to be able to collect feedback from the community and being
more open.

### Enhanced QA site

With the goal of providing more coherent information the QA test cases and results are now provided by the
[QA Report App](https://qa.apertis.org), which was integrated with the previous [lavaphabbridge](https://lavaphabbridge.apertis.org). This results in a better user experience and reduces
the costs associated with maintaining multiple different applications/repositories to view test cases and track reports.

## Documentation and designs

### Apertis test strategy

This release includes the new concept document [Apertis test strategy]({{< ref "test-strategy.md" >}})
advertising the test strategy Apertis applies to provide a high quality distribution and explaining
the different testing loops in the development and release workflow.

### Documentation refresh to match the switch to Flatpak

Continuing with the effort of updating documentation after switching to Flatpak, this
release also includes a reviewed version of several documents related to the Application
Framework to provide developers with the best practices to build their applications.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations

### Breaks

No know breaks

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2023dev2-testcases-builder),
Docker images.

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

{{% notice warning %}}

After intensive testing in LAVA on both v2023dev2 and v2023dev1 we've detected a very rare filesystem error when using OSTree images. This issue is very hard to reproduce and still under investigation but the system should be able to recover automatically from it.

{{% /notice %}}

### High (22)
 - [T8456](https://phabricator.apertis.org/T8456)	aum-api: test failed
 - [T8470](https://phabricator.apertis.org/T8470)	secure-boot-imx6 test failed 
 - [T8530](https://phabricator.apertis.org/T8530)	evolution-sync-bluetooth test not available in v2022 and v2023dev1 testcase page
 - [T8545](https://phabricator.apertis.org/T8545)	BOM file generation pick default license
 - [T8547](https://phabricator.apertis.org/T8547)	sanity-check: test failed
 - [T8603](https://phabricator.apertis.org/T8603)	AUM fails to detect rollback on Lava for RPi64 board
 - [T8604](https://phabricator.apertis.org/T8604)	AUM tests fails on Lava for RPi64 board
 - [T8660](https://phabricator.apertis.org/T8660)	Random FS issues on OSTree images
 - [T8689](https://phabricator.apertis.org/T8689)	Branching for v2023dev2 disabled CI for several projects
 - [T8729](https://phabricator.apertis.org/T8729)	"Segmentation fault" observed during enabling BT and Wifi dongle
 - [T8749](https://phabricator.apertis.org/T8749)	aum-offline-upgrade-signed: test failed
 - [T8757](https://phabricator.apertis.org/T8757)	aum-ota-api: test failed
 - [T8762](https://phabricator.apertis.org/T8762)	aum-ota-signed: test failed
 - [T8763](https://phabricator.apertis.org/T8763)	aum-out-of-space: test failed
 - [T8764](https://phabricator.apertis.org/T8764)	dbus-dos-reply-time: test failed
 - [T8765](https://phabricator.apertis.org/T8765)	aum-ota-auto: test failed
 - [T8772](https://phabricator.apertis.org/T8772)	aum-power-cut: test failed
 - [T8789](https://phabricator.apertis.org/T8789)	API test
 - [T8790](https://phabricator.apertis.org/T8790)	aum-ota-rollback-blacklist: test failed
 - [T8803](https://phabricator.apertis.org/T8803)	QA Report App: Pipeline fails to test MR
 - [T8822](https://phabricator.apertis.org/T8822)	aum-offline-upgrade: test failed
 - [T8823](https://phabricator.apertis.org/T8823)	connman: test failed

### Normal (41)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7817](https://phabricator.apertis.org/T7817)	rhosydd: test failed
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7852](https://phabricator.apertis.org/T7852)	Investigate test failure TestGetSourceMount
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T7945](https://phabricator.apertis.org/T7945)	evolution-sync-bluetooth test fails 
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8504](https://phabricator.apertis.org/T8504)	LAVA/Phab bridge timeouts
 - [T8516](https://phabricator.apertis.org/T8516)	apparmor-pipewire: test failed
 - [T8524](https://phabricator.apertis.org/T8524)	grilo: test failed
 - [T8572](https://phabricator.apertis.org/T8572)	Missing warning on coreutils overwrite
 - [T8613](https://phabricator.apertis.org/T8613)	apparmor-functional-demo: test failed
 - [T8622](https://phabricator.apertis.org/T8622)	Manual testcase results should not have any hyperlink in the LavaPhabridge report page
 - [T8629](https://phabricator.apertis.org/T8629)	frome: test failed
 - [T8634](https://phabricator.apertis.org/T8634)	Failed to start Access poi…server : logs seen in v2023dev1 Amd64 boot logs
 - [T8667](https://phabricator.apertis.org/T8667)	cgroups-resource-control: test failed
 - [T8668](https://phabricator.apertis.org/T8668)	Test apparmor-chaiwala-system does not work properly on OSTree images
 - [T8683](https://phabricator.apertis.org/T8683)	"firmware: failed to load" logs seen during boot
 - [T8748](https://phabricator.apertis.org/T8748)	sdk-docker: test failed

