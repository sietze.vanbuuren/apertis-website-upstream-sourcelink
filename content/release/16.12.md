+++
weight = 100
title = "16.12"
+++

# Release 16.12

- {{< page-title-ref "/release/16.12/release_schedule.md" >}}
- {{< page-title-ref "/release/16.12/releasenotes.md" >}}
