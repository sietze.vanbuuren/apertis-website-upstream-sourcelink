+++
date = "2022-06-03"
weight = 100

title = "v2021.6 Release schedule"
+++

The v2021.6 release cycle is scheduled to start in July 2022.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2022-07-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2022-08-10        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2022-08-17        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2022-08-24        |
| RC testing                                                                                               | 2022-08-25..09-07 |
| v2021.6 release                                                                                          | 2022-09-08        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
