+++
date = "2018-01-03"
weight = 100

title = "18.03 Release schedule"

aliases = [
    "/old-wiki/18.03/Release_schedule"
]
+++

The 18.03 release cycle starts in January 2018.

| Milestone                                                                                                | Date                  |
| -------------------------------------------------------------------------------------------------------- | --------------------- |
| Start of release cycle                                                                                   | 2018-01-01            |
| Soft feature freeze: end of feature proposal and review period                                           | 2018-02-16            |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2018-03-02            |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2018-03-09 EOD        |
| RC1 testing                                                                                              | 2018-03-12/2018-03-13 |
| 18.03 release                                                                                            | 2018-03-19            |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline
