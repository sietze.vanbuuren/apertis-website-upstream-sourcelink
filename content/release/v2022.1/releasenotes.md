+++
date = "2022-06-03"
weight = 100

title = "v2022.1 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022.1** is the second **stable** release of the Apertis v2022
stable [release flow]( {{< ref "release-flow.md#apertis-release-flow" >}} ).
Apertis is committed to maintaining the v2022 release stream up to the end
of 2023.

This Apertis release is built on top of Debian Bullseye with several
customisations and the Linux kernel 5.15.x LTS series.

Test results for the v2022.1 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2022/20220601.0119/apt)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2022/20220601.0119/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2022/20220601.0119/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2022/20220601.0119/lxc)

## Release flow

  - 2020 Q4: v2022dev0
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - **2022 Q2: v2022.1**
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022.1 images](https://images.apertis.org/release/v2022/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [fixedfunction](https://images.apertis.org/release/v2022/v2022.1/amd64/fixedfunction/apertis_ostree_v2022-fixedfunction-amd64-uefi_v2022.1.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.1/amd64/hmi/apertis_ostree_v2022-hmi-amd64-uefi_v2022.1.img.gz) | [base SDK](https://images.apertis.org/release/v2022/v2022.1/amd64/basesdk/apertis_v2022-basesdk-amd64-sdk_v2022.1.ova) | [SDK](https://images.apertis.org/release/v2022/v2022.1/amd64/sdk/apertis_v2022-sdk-amd64-sdk_v2022.1.ova)
| ARM 32-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.1/armhf/fixedfunction/apertis_ostree_v2022-fixedfunction-armhf-uboot_v2022.1.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.1/armhf/hmi/apertis_ostree_v2022-hmi-armhf-uboot_v2022.1.img.gz)
| ARM 64-bit (U-Boot)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.1/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-uboot_v2022.1.img.gz)
| ARM 64-bit (Raspberry Pi)	| [fixedfunction](https://images.apertis.org/release/v2022/v2022.1/arm64/fixedfunction/apertis_ostree_v2022-fixedfunction-arm64-rpi64_v2022.1.img.gz) | [hmi](https://images.apertis.org/release/v2022/v2022.1/arm64/hmi/apertis_ostree_v2022-hmi-arm64-rpi64_v2022.1.img.gz)

The Intel `fixedfunction` and `hmi` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022.1 repositories

    deb https://repositories.apertis.org/apertis/ v2022 target development sdk


## Changes

This is a point release in the stable cycle, only security fixes and
small changes are appropriate for this release stream.

This release includes the security updates from Debian Bullseye and the latest
LTS Linux kernel on the 5.15.x series.

## Deprecations and ABI/API breaks

### Regressions

No known regressions.

### Deprecations

No known deprecations.

### Breaks

No known breaks.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure repository](https://build.collabora.com/project/show/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at <https://images.apertis.org/>

### Infrastructure overview

The [Apertis Image Recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes/)
provides an overview of the image building process and the involved
services.

## Known issues

{{% notice warning %}}

After intensive testing in LAVA on both v2022.0 and v2022.1 we've detected a very rare filesystem error when using OSTree images. This issue is very hard to reproduce and still under investigation but the system should be able to recover automatically from it.

{{% /notice %}}

### High (24)
 - [T7879](https://phabricator.apertis.org/T7879)	sdk-debos-image-building: test failed
 - [T8456](https://phabricator.apertis.org/T8456)	aum-api: test failed
 - [T8547](https://phabricator.apertis.org/T8547)	sanity-check: test failed
 - [T8603](https://phabricator.apertis.org/T8603)	AUM fails to detect rollback on Lava for RPi64 board
 - [T8604](https://phabricator.apertis.org/T8604)	AUM tests fails on Lava for RPi64 board
 - [T8660](https://phabricator.apertis.org/T8660)	Random FS issues on OSTree images
 - [T8749](https://phabricator.apertis.org/T8749)	aum-offline-upgrade-signed: test failed
 - [T8757](https://phabricator.apertis.org/T8757)	aum-ota-api: test failed
 - [T8762](https://phabricator.apertis.org/T8762)	aum-ota-signed: test failed
 - [T8763](https://phabricator.apertis.org/T8763)	aum-out-of-space: test failed
 - [T8765](https://phabricator.apertis.org/T8765)	aum-ota-auto: test failed
 - [T8772](https://phabricator.apertis.org/T8772)	aum-power-cut: test failed
 - [T8789](https://phabricator.apertis.org/T8789)	Gitlab issues test: API Test for Gitlab issues
 - [T8790](https://phabricator.apertis.org/T8790)	aum-ota-rollback-blacklist: test failed
 - [T8822](https://phabricator.apertis.org/T8822)	aum-offline-upgrade: test failed
 - [T8825](https://phabricator.apertis.org/T8825)	aum-ota-out-of-space: test failed
 - [T8827](https://phabricator.apertis.org/T8827)	aum-rollback-blacklist: test failed
 - [T8840](https://phabricator.apertis.org/T8840)	Gitlab issues test: Audio is not heard on startup
 - [T8842](https://phabricator.apertis.org/T8842)	gstreamer1-0-decode: test failed
 - [T8868](https://phabricator.apertis.org/T8868)	Strace package is being failed on OBS
 - [T8896](https://phabricator.apertis.org/T8896)	aum-offline-upgrade-branch: test failed
 - [T8899](https://phabricator.apertis.org/T8899)	disk-rootfs-fsck: test failed
 - [T8925](https://phabricator.apertis.org/T8925)	ostree-push failure is seen when trying to Reproduce a build 
 - [T8927](https://phabricator.apertis.org/T8927)	Flatpak: sdk-flatpak-build-helloworld-app test fails on v2021.5 sdk

### Normal (42)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing 
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T6024](https://phabricator.apertis.org/T6024)	sdk-dbus-tools-d-feet: folks-inspect: command not found 
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7721](https://phabricator.apertis.org/T7721)	Fakemachine in debos immediately powers off and hangs in v2021 and v2022dev1 when using UML on the runners 
 - [T7785](https://phabricator.apertis.org/T7785)	DNS over TLS does not work on systemd-resolve
 - [T7819](https://phabricator.apertis.org/T7819)	newport: test failed
 - [T7852](https://phabricator.apertis.org/T7852)	Investigate test failure TestGetSourceMount
 - [T7859](https://phabricator.apertis.org/T7859)	spymemcached: Investigate failing test due to hostname mismatch
 - [T7872](https://phabricator.apertis.org/T7872)	Error building package ruby-redis on OBS
 - [T7923](https://phabricator.apertis.org/T7923)	Gitlab pipeline OBS job reported "success" but OBS build was "unresolvable"
 - [T8175](https://phabricator.apertis.org/T8175)	License scan fails on package texlive-extra
 - [T8194](https://phabricator.apertis.org/T8194)	ci-license-scan prints final error paragraph in the middle of scan-copyrights output
 - [T8281](https://phabricator.apertis.org/T8281)	dashboard: gnome-settings-daemon-data incorrectly flagged as missing the git repository
 - [T8504](https://phabricator.apertis.org/T8504)	LAVA/Phab bridge timeouts
 - [T8524](https://phabricator.apertis.org/T8524)	grilo: test failed
 - [T8572](https://phabricator.apertis.org/T8572)	Missing warning on coreutils overwrite
 - [T8622](https://phabricator.apertis.org/T8622)	Manual testcase results should not have any hyperlink in the LavaPhabridge report page
 - [T8629](https://phabricator.apertis.org/T8629)	frome: test failed
 - [T8634](https://phabricator.apertis.org/T8634)	Failed to start Access poi…server : logs seen in v2023dev1 Amd64 boot logs
 - [T8668](https://phabricator.apertis.org/T8668)	Test apparmor-chaiwala-system does not work properly on OSTree images
 - [T8683](https://phabricator.apertis.org/T8683)	"firmware: failed to load" logs seen during boot
 - [T8824](https://phabricator.apertis.org/T8824)	The apertis-flatdeb-mildenhall pipeline is flaky
 - [T8841](https://phabricator.apertis.org/T8841)	apparmor-ofono: test failed
 - [T8860](https://phabricator.apertis.org/T8860)	Copyrights in the image licenses.json file contain entries with newlines
 - [T8874](https://phabricator.apertis.org/T8874)	The firefox-esr package is being failed on OBS
 - [T8875](https://phabricator.apertis.org/T8875)	Terminal at xfce desktop corrupted after debos call
 - [T8887](https://phabricator.apertis.org/T8887)	Some binaries in the toolchain tarball are huge
 - [T8909](https://phabricator.apertis.org/T8909)	apparmor-pipewire: test failed
 - [T8924](https://phabricator.apertis.org/T8924)	Define strategy for FS corruption issue

