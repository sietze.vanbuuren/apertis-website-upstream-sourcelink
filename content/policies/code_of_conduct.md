+++
date = "2015-12-16"
weight = 100

title = "Code of Conduct"

aliases = [
    "/old-wiki/Apertis:Code_of_conduct",
    "/old-wiki/Apertis:Code_of_conductpage"
]
+++

This text documents what you can expect from the Apertis community
online and at events and, in turn, what we expect from you.

We believe that people generally mean well. Excepting strong evidence to
the contrary, we ask that you do the same. We favour communication over
confrontation. Most issues that arise during day-to-day contribution to
any software project and at events can be resolved by communication
between the affected parties.

People make mistakes. Good people are capable of engaging in thoughtless
actions and are capable of making thoughtless remarks. Sometimes these
actions and words cross a line. Even if you are not yourself offended by
this behaviour, we ask that you discretely remind people that others may
be. We do not engage in public shaming of any kind and we ask the same
from you.

We expect everyone to treat others with respect, no matter their level
of contribution to the project.

In the event of serious offences or in case you do not feel comfortable
confronting someone yourself, some individuals that you can approach for
help can be designated. These people can help mediate conflicts or can
simply take an individual aside and let them know that their behaviour
was unacceptable. Your privacy will be maintained to the degree that you
request. In very serious situations, we will help you with contacting
the local police and filing a report where possible.

Apertis does not engage in discrimination. Everyone is welcome in all
community communication channels and at all events.

## Who can I approach?

Senior representatives of the Apertis project can be contacted via
email at [conduct@apertis.org](mailto:conduct@apertis.org)

## Credits

*The first version of this page was based on a code of conduct by
Allison Ryan Lortie \<desrt@desrt.ca\>*
